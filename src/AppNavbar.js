// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'

import {Fragment, useState} from 'react'
import {Navbar, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom';

export default function AppNavbar(){


	return(
	<Navbar bg="light" expand="lg">
	  <Navbar.Brand as={Link} to="/" exact>Zuitt</Navbar.Brand>
	  <Navbar.Toggle aria-controls="basic-navbar-nav" />
	  <Navbar.Collapse id="basic-navbar-nav">
	    <Nav className="mr-auto">
	      <Nav.Link as={Link} to="/home" exact>Home</Nav.Link>
	      <Nav.Link as={Link} to="/courses" exact>Courses</Nav.Link>
	      <Nav.Link as={Link} to="/register" exact>Register</Nav.Link>
	      <Nav.Link as={Link} to="/login" exact>Login</Nav.Link>

	    </Nav>
	  </Navbar.Collapse>
	</Navbar>

	)
}